//console.log("Hello, world!");

let pix_real = 0;
let pix_imag = 0;
let iteration = 0;
let z_real = 0.0;
let z_imag = 0.0;
let w_real = 0.0;
let w_imag = 0.0;
let c_real = 0.0;
let c_imag = 0.0;
let line = "";
let bailout = false;
const maxIterations = 1000000;
let totalIterationCount = 0;

for (pix_imag = 19; pix_imag >= -20; pix_imag--) {
    c_imag = pix_imag / 20;
    line = "";
    for (pix_real = -40; pix_real <= 39; pix_real++) {
        bailout = false;
        c_real = pix_real / 40 - 0.5;
        z_real = 0.0;
        z_imag = 0.0;
        for (iteration = 0; ((iteration < maxIterations) && !bailout); iteration++) {
			w_real = (z_real * z_real) - (z_imag * z_imag) + c_real;
			w_imag = (2 * z_real * z_imag) + c_imag;
			z_real = w_real;
			z_imag = w_imag;
			if (z_real * z_real + z_imag * z_imag >= 4) {bailout = true;}
			totalIterationCount++; 
        }
		if (iteration > 32) {
			line += " ";
		}
		else if (iteration > 16) {
			line += ".";	
		}
        else if (iteration > 8) {
            line += "o";
        }
        else if (iteration > 4) {
            line += "O";
        }
        else {
            line += "X";
        }
    }
    console.log(line);
}
